<!DOCTYPE html>
<html lang="en">
<head>
	<title>The Solar Family - <?php echo $section;?></title>
	<link href='http://fonts.googleapis.com/css?family=Fredericka+the+Great' rel='stylesheet' type='text/css'>
	<script href="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">
  <!--<script src="http://code.jquery.com/jquery-1.9.1.js"></script>-->
  <script type="text/javascript" src="jquery-1.10.2.min.js"></script>
  <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
  <script href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css"></script>
  <script type="text/javascript" src="jcarousellite_1.0.1.min.js"></script>
  <!-- Bootstrap core CSS -->
  <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">
  <!-- Animate CSS -->
  <link rel="stylesheet" type="text/css" src="animate-custom.css">

  <!-- Custom styles for this template -->
  <link rel="stylesheet" type="text/css" href="fam_css.css">
</head>

<body>
	<div class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">The Solar Family</a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="fam_index.php">Home</a></li>
            <li><a href="meet.php">Meet The Family</a></li>
            <li><a href="solarblog.php">Solar Talk</a></li>
            <li><a href="#contact">Photo Album</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>