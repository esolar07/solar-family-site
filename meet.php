<?php $section='Meet the Family';?>
<?php include 'fheader.php';?>
	<div class="lined">
	    <div class="container">

	      <div class="starter-template">
	        <h1><span style="font-size:60px">Meet the Family</span></h1>
	       	<div class="carousel">
	            <ul>
	                <li><img class="picc" src="https://scontent-a-mia.xx.fbcdn.net/hphotos-prn2/1461116_10202489249015606_1078916989_n.jpg" alt="" width="200" height="200" ></li>
	                <li><img class="picc" src="https://scontent-a-mia.xx.fbcdn.net/hphotos-ash2/285597_10200742201740516_2093512943_n.jpg" alt="" width="200" height="200" ></li>
	                <li><img class="picc" src="https://scontent-b-mia.xx.fbcdn.net/hphotos-prn1/314331_4484444952330_1894481363_n.jpg" alt="" width="200" height="200" ></li>
	                <li><img class="picc" src="https://scontent-a-mia.xx.fbcdn.net/hphotos-ash3/576753_4082069733201_1976426258_n.jpg" alt="" width="200" height="200" ></li>
	                <li><img class="picc" src="https://scontent-a-mia.xx.fbcdn.net/hphotos-prn1/48079_10200705417060922_2044474816_n.jpg" alt="" width="200" height="200" ></li>
	                <li><img class="picc" src="https://fbcdn-sphotos-d-a.akamaihd.net/hphotos-ak-frc3/425875_4474415893992_1004240216_n.jpg" alt="" width="200" height="200" ></li>
	                <li><img class="picc" src="https://scontent-b-mia.xx.fbcdn.net/hphotos-frc3/292704_4910133994290_1099984919_n.jpg" alt="" width="200" height="200" ></li>
	                <li><img class="picc" src="https://scontent-b-mia.xx.fbcdn.net/hphotos-frc3/574998_4570261897700_1517584564_n.jpg" alt="" width="200" height="200" ></li>
	            </ul>
        	</div>
	    	<script type="text/javascript">
	    		$(".carousel").jCarouselLite({
	                auto: 800,
	                speed: 1000
	            });
	        </script>
	        <div id="tabs">
			  <ul>
			    <li><a href="#fragment-1"><span class="tab">Eddie</span></a></li>
			    <li><a href="#fragment-2"><span class="tab">Kate</span></a></li>
			    <li><a href="#fragment-3"><span class="tab">Juliana</span></a></li>
			    <li><a href="#fragment-4"><span class="tab">Lauren</span></a></li>
			    <li><a href="#fragment-5"><span class="tab">Elizabeth</span></a></li>
			    <li><a href="#fragment-6"><span class="tab">Aiden</span></a></li>
			  </ul>
			  <div id="fragment-1">
			    <img class="tabpic" src="http://mdak.on.com/md/4/9cu5tea73hnv8ce1c2kz.jpg" width='200' height='200'>
			    <div class="div1">
			    	<h3>Eddie Solar</h3>
				</div>
			    <div class="div2">
				    <p>Birth Date: July 1, 1983</p>
				 	<p>Birth Place: Grand Rapids, MI</p>
				    <p>Interests:</p>
				</div>
			  </div>
			  <div id="fragment-2">
			  	<img class="tabpic"src="https://fbcdn-profile-a.akamaihd.net/hprofile-ak-ash3/s160x160/599820_10202364195689351_388240302_a.jpg" width='200' height='200'>
			  	<h3>Kate Elizabeth Blaum</h3>
			    <p>Birth Date: March 4, 1983</p>
			    <p>Birth Place: Lincoln, IL</p>
			    <p>Interests:</p>
			  </div>
			  <div id="fragment-3">
			    <img class="tabpic"src="https://scontent-a-mia.xx.fbcdn.net/hphotos-ash2/417431_4012919796878_1495616931_n.jpg" width='200' height='200'>
			  	<h3>Juliana Nicole Blaum</h3>
			    <p>Birth Date: </p>
			    <p>Birth Place: Springfield, IL</p>
			    <p>Interests: ONE DIRECTION, Food, T.V., Roller Skating, Swimming, Sleeping, School</p>
			    <p>Quote: "I hate clowns..."</p>
			  </div>
			  <div id="fragment-4">
			    <img class="tabpic" src="https://scontent-b-mia.xx.fbcdn.net/hphotos-prn1/561345_4012914436744_431359332_n.jpg" width='200' height='200'>
			  	<h3>Lauren Jhanelle Solar</h3>
			    <p>Birth Date: December 6, 2004</p>
			    <p>Birth Place: Miami, FL</p>
			    <p>Interests: Teen Beach Movie, ONE DIRECTION, Monster High, puppies, T.V., Swimming, Walking,</p>
			    <p>Quote: "I hate scary stuff..."</p>
			  </div>
			  <div id="fragment-5">
			    <img class="tabpic" src="https://scontent-a-mia.xx.fbcdn.net/hphotos-ash2/582216_4012912076685_1689049886_n.jpg" width='200' height='200'>
			  	<h3>Elizabeth Marie Solar</h3>
			    <p>Birth Date: March 14, 2006</p>
			    <p>Birth Place: Miami, FL</p>
			    <p>Interests: Teen Beach Movie, ONE DIRECTION, LaLaLoopsies, puppies, Barbie Dolls, Power Rangers (pink), Candy Land, Jump A Roos</p>
			    <p>Quote: "I don't like walking in the dark..." </p>
			  </div>
			  <div id="fragment-6">
			    <img class="tabpic" src="https://scontent-a-mia.xx.fbcdn.net/hphotos-ash2/527923_4012897156312_702720035_n.jpg" width='200' height='200'>
			  	<h3>Aiden Xander Berrios</h3>
			    <p>Birth Date: February 21, 2009</p>
			    <p>Birth Place: Miami. FL</p>
			    <p>Interests: Ninjago, Spider-Man, Avengers, Super Man, Power Rangers, Nintendo DS, Swimming, Cookies</p>
			    <p>Quote: "Do it like a bossss..."</p>
			  </div>
			</div>
			 
			 <!-- JS for tabs -->
			<script>
			$( "#tabs" ).tabs();
			</script>

	    	<script type="text/javascript">
	    	$document.ready(function(){
	    		$(".div1").click(function(){
	    			$(".div2").slidetoggle("slow");
	   	    	})
	   	    })	
	    	</script>


	    </div><!-- /.container -->
		</div>
	<?php include 'ffooter.php'; ?>